import Image from "next/image";
import splash from '../../public/1.jpg'
import {Love_Ya_Like_A_Sister} from "next/font/google";



const loveFont = Love_Ya_Like_A_Sister({subsets:['latin'], weight: "400"})

export default function Splash(){

    return(
        <main className={`h-screen flex flex-col items-center justify-center`}>
            <Image className={`rounded-md`} src={splash} alt={``} height={500}/>
            <br/>
            <br/>
            <h1 className={`${loveFont.className} text-center text-4xl`}>YAY!!!! YEEEEEEY 😍 🥰 😘 THANK YOU</h1>
        </main>
    )
}
