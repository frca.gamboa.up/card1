'use client'
import {useState} from "react";
import Splash from "./Splash";
import {Love_Ya_Like_A_Sister} from "next/font/google";
import Image from "next/image";
import image2 from '../../public/2.jpg'
import image3 from '../../public/3.jpg'
import image4 from '../../public/4.jpg'
import image5 from '../../public/5.jpg'
import image6 from '../../public/6.jpg'
import image7 from '../../public/7.jpg'
import image8 from '../../public/8.jpg'
import image9 from '../../public/9.jpg'
import image10 from '../../public/10.jpg'
import image11 from '../../public/11.jpg'



const loveFont = Love_Ya_Like_A_Sister({subsets:['latin'], weight: "400"})

export default function Home() {
    const [fontSize, setFontSize] = useState<number>(1)
    const [finalState, setFinalState] = useState<boolean>(false)
    const [index, setIndex] = useState<number>(0)
    const  [imageIndex, setImageIndex] = useState<number>(0)
    const phrases : string[] = [
        'No',
        'Are u sure?',
        'Really sure?',
        'Please Think about it','Sige na Payag na ako..',
        'if you say no, ill very sad',
        'if you say no, ill very very sad',
        'Pretty Please :(',
        'Dont do this to me',
        "You're Breaking my Heart",'Im gonna cry.....',
        'Im begging you',
        'Sige na','Please ang Pangit ka bonding'

    ]
    const images = [image2,image3,image4,image5,image6,image7,image8,image9,image10,image11]

    return (
        <>
            {finalState ? <Splash/> :

                <div className="flex gap-5 flex-col justify-center items-center">
                    <Image className={`rounded-md`} src={images[(imageIndex % images.length)]} alt={``} height={390}/>
                    <h1 className={`${loveFont.className} text-4xl text-center`}>Will you be my valentine?</h1>
                    <div className="flex flex-col items-center justify-center gap-2">
                        <button style={{fontSize: `${fontSize}em`}}
                                className={`bg-green-600 text-white rounded-md px-[1em] py-[0.5em] font-bold`}
                                onClick={() => {
                                    setFinalState(true)
                                }}>Yes
                        </button>
                        <button className={`bg-red-600  text-white px-[1em] py-[0.5em] rounded-md font-bold`}
                                onClick={() => {
                                    setFontSize(fontSize + .5);
                                    setIndex(index + 1);
                                    setImageIndex(imageIndex + 1)
                                }}
                        >{phrases[(index % phrases.length)]}
                        </button>
                    </div>


                </div>}
        </>
    )
}
